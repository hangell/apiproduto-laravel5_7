<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//'id', 'name', 'price', 'description', 'slug'

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'description', 'slug'
    ];
}
